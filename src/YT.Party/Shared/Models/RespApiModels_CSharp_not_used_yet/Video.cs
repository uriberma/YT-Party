﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YT.Party.Shared.Models.RespApiModels
{
    /*
     type Video =
        { 
          VideoId : string
          Id : int
          StartSeconds : int
          EndSeconds : int
        }
     */
    public class Video
    {
        public string VideoId { get; set; }
        public int Id { get; set; }
        public int StartSeconds { get; set; }
        public int EndSeconds { get; set; }
    }
}
