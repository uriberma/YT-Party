﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YT.Party.Shared.Models.RespApiModels
{   /*
        type AddVideo =
            { 
              VideoLink : string
              StartSeconds : int
              EndSeconds : int
            } 
     
    */
    public class AddVideo
    {
        public string VideoLink { get; set; }
        public int StartSeconds { get; set; }
        public int EndSeconds { get; set; }
    }
}
