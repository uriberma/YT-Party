﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YT.Party.Shared.Models.RespApiModels
{
    /*
        type PartyResult =
            { 
              PartyPlaylist : string
             
              PartyPlayer : string
            }
     */
    public class PartyResult
    {
        public string PartyPlayList { get; set; }
        public string PartyPlayer { get; set; }
    }
}
