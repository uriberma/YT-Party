﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YT.Party.Shared.Models.RespApiModels
{
    /*
     type Session =
        {
          Password : string
          SessionId : string
          UsersCanAdd : bool
          UsersCanDelete : bool
          UsersCanAccessPlayer : bool
        }
     */
    public class Session
    {
        public string Password { get; set; }
        public string SessionId { get; set; }
        public bool UserCanAdd { get; set; }
        public bool UserCanDelete { get; set; }
        public bool UserCanAccessPlayer { get; set; }
    }
}
