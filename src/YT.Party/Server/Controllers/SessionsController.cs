﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace YT.Party.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SessionsController : ControllerBase
    {
        // POST api/<SessionsController>/createSession/
        [HttpPost("createSession")]
        public Session.DjSession Post([FromBody] Models.Session session)
        {
            return Session.updateSession(session, Session.DjSession.Empty);
        }

        // GET: api/<SessionsController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<SessionsController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

       

        // PUT api/<SessionsController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<SessionsController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
