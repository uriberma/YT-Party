﻿namespace YT.Party

module Uri =
  let mkUri r = System.Uri r
  type UriError =
    // Unable to parse the given get parameter (expected something like 'v=test')
    | ParseGetParameterError of string
  let queryDict (uri:System.Uri) =
    uri.Query.Split([|"&"; "?"|], System.StringSplitOptions.RemoveEmptyEntries)
    |> Seq.map (fun q ->
        let kv = q.Split([|"="|], System.StringSplitOptions.None)
        if kv.Length = 2 then
          Result.succeed (kv.[0], kv.[1])
        else
          Result.fail (ParseGetParameterError q))
    |> Result.collectIgnoreErrors
    |> Result.map dict

module Youtube =
  type YoutubeId = YoutubeId of string
  type YoutubeError =
    | UriError of Uri.UriError
    // Could not parse the given link
    | UnknownHost of string
    // Could not find the 'v=' parameter in the link
    | VideoIdParameterMissing of string

  // For example
  // https://www.youtube.com/watch?v=Ahwc-ouFeTQ
  // https://youtu.be/Ahwc-ouFeTQ
  let tryParseLink link =
    match System.Uri.TryCreate(link, System.UriKind.Absolute) with
    | true, uri ->
      if uri.Host.EndsWith "youtube.com" then
        Uri.queryDict uri
        |> Result.bindAdvS UriError (fun d ->
          match d.TryGetValue "v" with
          | true, v -> Result.succeed v
          | _ -> Result.fail (VideoIdParameterMissing link))
        |> Result.map YoutubeId
      elif uri.Host.EndsWith "youtu.be" then
        match uri.Segments |> Seq.tryLast with
        | Some last -> Result.succeed (YoutubeId last)
        | None -> Result.fail (VideoIdParameterMissing link)
      else
        Result.fail (UnknownHost link)
    | _ -> Result.fail (UnknownHost link)
    //None : YoutubeId option

module MyVideo =
  type MyVideoLink = MyVideoLink of string
  let tryParseLink link =
    Result.fail ""