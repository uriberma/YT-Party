﻿namespace YT.Party.Models

type ErrorViewModel() =
    member val StatusCode = "" with get, set
    member val Exception = null : exn with get, set