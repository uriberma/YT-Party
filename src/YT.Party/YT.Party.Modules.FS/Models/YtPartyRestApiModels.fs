﻿namespace YT.Party.Models

open System.Runtime.Serialization

[<DataContract>]

// https://developers.google.com/youtube/iframe_api_reference
type Video =
    { [<field:DataMember(Name = "videoId")>]
      VideoId : string
      [<field:DataMember(Name = "id")>]
      Id : int
      [<field:DataMember(Name = "startSeconds")>]
      StartSeconds : int
      [<field:DataMember(Name = "endSeconds")>]
      EndSeconds : int}

[<DataContract>]
type AddVideo =
    { [<field:DataMember(Name = "videoLink")>]
      VideoLink : string
      [<field:DataMember(Name = "startSeconds")>]
      StartSeconds : int
      [<field:DataMember(Name = "endSeconds")>]
      EndSeconds : int
      }

[<DataContract>]
type PartyResult =
    { [<field:DataMember(Name = "partyPlaylist")>]
      PartyPlaylist : string
      [<field:DataMember(Name = "partyPlayer")>]
      PartyPlayer : string }

[<DataContract>]
type Session =
    { [<field:DataMember(Name = "password")>]
      Password : string
      [<field:DataMember(Name = "sessionId")>]
      SessionId : string
      [<field:DataMember(Name = "usersCanAdd")>]
      UsersCanAdd : bool
      [<field:DataMember(Name = "usersCanDelete")>]
      UsersCanDelete : bool
      [<field:DataMember(Name = "usersCanAccessPlayer")>]
      UsersCanAccessPlayer : bool }

[<DataContract>]
type PlayerPosition =
    { [<field:DataMember(Name = "playerPosition")>]
      PlayerPosition : int }