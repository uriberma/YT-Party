﻿namespace YT.Party

type Result<'T, 'E> =
  | Success of 'T * ('E list)
  | Error of 'E list

module Result =
  let succeed r = Success (r, [])
  let succeedWith r e = Success (r, e)
  let fail r = Error [r]
  let isSuccess = function
    | Success _ -> true
    | _ -> false
  let asOption = function
    | Success (a, _) -> Some a
    | _ -> None
  let isError (x:Result<_,_>) = not (isSuccess x)
  let getErrors = function
    | Error a
    | Success (_, a) -> a
  let combine x = function
    | Success (b, eb) ->
      match x with
      | Success (a, ea) -> Success(Choice1Of3 (a, b), eb @ ea)
      | Error ea -> Success (Choice3Of3 b, eb @ ea)
    | Error eb ->
      match x with
      | Success (a, ea) -> Success(Choice2Of3 a, eb @ ea)
      | Error ea -> Error (eb @ ea)
  let bindAdv fe f = function
    | Success (a, ea) ->
      match f a with
      | Success (b, eb) -> Success(b, eb @ fe ea)
      | Error eb -> Error (eb @ fe ea)
    | Error ea -> Error (fe ea)
  let bindAdvS fe = bindAdv (List.map fe)
  let bind f = bindAdv id f
  let mapAdv fe f = function
    | Success (a, ea) -> Success (f a, fe ea)
    | Error ea -> Error (fe ea)
  let map f = mapAdv id f
  let mapError f = function
    | Success (a, ea) -> Success (a, f ea)
    | Error ea -> Error (f ea)
  let mapErrors f = mapError (List.map f)

  let recover f = function
    | Success (a, ea) -> Success(a, ea)
    | Error (ea) ->
      match f () with
      | Success (b, eb) -> Success(b, eb @ ea)
      | Error eb -> Error (eb @ ea)
  let recoverAdv f = function
    | Success (a, ea) -> Success(a, ea)
    | Error (ea) -> f ea

  let collect s =
    let cached =
      s
      |> Seq.scan (fun (_, s) item ->
        let next = s |> bind (fun current -> map (fun i -> i :: current) item)
        isSuccess s && isError next, next
      ) (false, succeed [])
      |> Seq.cache
    match cached
          |> Seq.tryFind fst with // Stop processing as soon as we hit the last item.
    | Some s -> s
    | None -> cached |> Seq.last
    |> snd
    |> map List.rev

  let collectAllErrors s =
    s
    |> Seq.fold (fun s item ->
      match combine item s with
      | Success (Choice1Of3 (a, l), e) -> Success (a::l, e)
      | Success (_, e)
      | Error e -> Error e) (succeed [])
    |> map List.rev

  let collectIgnoreErrors s =
    s
    |> Seq.fold (fun (li, errs) item ->
      match item with
      | Success (a, ea) -> a :: li, List.rev ea @ errs
      | Error ea -> li, List.rev ea @ errs
    ) ([], [])
    |> fun (li, errs) -> Success(li |> List.rev, errs |> List.rev)