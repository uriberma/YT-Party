﻿namespace YT.Party

module Session =
  type SessionError =
    | UriError of Uri.UriError
    | YoutubeIdMissing of string
    | MyVideoError of string
    | NoModuleCouldParseLink of string
    | Unauthorized
    | SessionModified
    | BadRequest of string

  let mapYoutubeErrors yterr =
    yterr
    |> List.choose (fun e ->
      match e with
      | Youtube.YoutubeError.UriError uriErr -> Some (UriError uriErr)
      | Youtube.YoutubeError.UnknownHost _ -> None
      | Youtube.YoutubeError.VideoIdParameterMissing uri -> Some (YoutubeIdMissing uri))
  let mapMyVideoError myVideo = [] // Ignroe all myvideo errors for now
  type VideoLink =
    | Youtube of Youtube.YoutubeId
    | MyVideo of MyVideo.MyVideoLink
    static member TryParse x =
      Youtube.tryParseLink x |> Result.mapAdv mapYoutubeErrors (Youtube)
      |> Result.recover (fun _ -> MyVideo.tryParseLink x |> Result.mapAdv mapMyVideoError MyVideo)
      |> Result.recover (fun _ -> Result.fail (NoModuleCouldParseLink x))
    member x.Id =
      match x with
      | Youtube (Youtube.YoutubeId id) -> id
      | _ -> failwith "not supported"
  type Video =
    { Id : int
      Link : VideoLink
      StartSeconds: int
      EndSeconds: int}
  let getVideoId v = v.Id
  type DjSession =
    { /// The Id the video running on the player
      PlayerPosition : int
      /// The next free id (for new videos to add)
      NextId : int
      /// The current playlist
      PlayList : Video list
      /// The admin password
      AdminPassword : string
      UsersCanAdd : bool
      UsersCanDelete : bool
      UsersCanAccessPlayer : bool }
    static member Empty =
      { PlayerPosition = 0
        NextId = 0
        PlayList = []
        AdminPassword = "admin"
        UsersCanAdd = false
        UsersCanDelete = false
        UsersCanAccessPlayer = false }
  type Sessions = Sessions of System.Collections.Concurrent.ConcurrentDictionary<string, DjSession>
  let createSessions () =
      Sessions (new System.Collections.Concurrent.ConcurrentDictionary<string, DjSession>())
  let checkAuthorized userAllowed followUp adminPassword s =
    if s.AdminPassword = adminPassword || userAllowed s then
      followUp s
    else
      Result.fail Unauthorized

  let addVideos (videos:Models.AddVideo seq) =
    checkAuthorized (fun s -> s.UsersCanAdd) (fun s ->
      videos
      |> Seq.map ((fun v -> v.VideoLink) >> VideoLink.TryParse)
      |> Result.collectAllErrors
      |> Result.map (fun vids ->
        let addedItems =
          vids
          |> List.mapi (fun i video -> { Id = s.NextId + i; Link = video; StartSeconds = (videos |> Seq.item i).StartSeconds; EndSeconds = (videos |> Seq.item i).EndSeconds})
        { s with
            NextId = s.NextId + vids.Length
            PlayList = s.PlayList @ addedItems }))

  let removeVideo (id:int) =
    checkAuthorized (fun s -> s.UsersCanDelete) (fun s ->
      let newPlayerPos =
        if s.PlayList.Length > 1 then
          Seq.append s.PlayList s.PlayList
          |> Seq.map getVideoId
          |> Seq.skipWhile ((<>) id)
          |> Seq.skip 1
          |> Seq.head
        else s.NextId
      let res = { s with PlayerPosition = newPlayerPos; PlayList = s.PlayList |> List.filter (getVideoId >> (<>) id) }
      // Make sure we don't have a 
      res |> Result.succeed)
  let removeAllVideos =
    checkAuthorized (fun s -> s.UsersCanDelete) (fun s ->
      { s with PlayList = []; PlayerPosition = s.NextId }
      |> Result.succeed)

  let setPlayerPos (pos:int) =
    checkAuthorized (fun s -> false) (fun s ->
      { s with
          PlayerPosition = pos }
      |> Result.succeed)

  let nextPlayerPos (oldPos:int) =
    checkAuthorized (fun s -> false) (fun s ->
      let curPos = s.PlayerPosition
      let nextPos =
        let indx = s.PlayList |> Seq.findIndex (getVideoId >> (=) oldPos)
        let newItem = Seq.item (indx + 1) (Seq.append s.PlayList s.PlayList)
        newItem.Id
      if oldPos = curPos then
        { s with
            PlayerPosition = nextPos }
        |> Result.succeed
      else
        Result.fail SessionModified)

  let getPlayerPos () =
    checkAuthorized (fun s -> true) (fun s ->
      s.PlayerPosition
      |> Result.succeed)

  let randomizePlaylist =
    checkAuthorized (fun s -> s.UsersCanAdd && s.UsersCanDelete) (fun s ->
      let r = new System.Random()
      { s with
          PlayList = (s.PlayList |> List.sortBy (fun _ -> r.NextDouble()))}
      |> Result.succeed)

  let setUserAccess canAdd canDelete canViewPlayer =
    checkAuthorized (fun s -> false) (fun s ->
      { s with
          UsersCanAccessPlayer = canViewPlayer
          UsersCanAdd = canAdd
          UsersCanDelete = canDelete }
      |> Result.succeed)

  let isPasswordInvalid s =
    System.String.IsNullOrWhiteSpace s

  let setPassword pass =
    checkAuthorized (fun s -> false) (fun s ->
      if isPasswordInvalid pass && s.AdminPassword <> pass then
        Result.fail (BadRequest "Password is invalid and was not changed")
      else
        { s with
            AdminPassword = pass }
        |> Result.succeed)

  let randomStr =
    let random = System.Random()
    fun (chars:string) len ->
      let charsLen = chars.Length
      let randomChars = [|for _ in 0..len -> chars.[random.Next(charsLen)]|]
      new System.String(randomChars)
  let regularChars = "ABCDEFGHIJKLMNOPQRSTUVWUXYZ0123456789"
  let extendedChars = "abcdefghijklmnopqdrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWUXYZ0123456789-_"
  let createSessionId () = randomStr extendedChars 2

  let updateSession (request:Models.Session) (previous:DjSession) =
      let pw =
        match isPasswordInvalid request.Password, isPasswordInvalid previous.AdminPassword with
        | false, _ -> request.Password
        | true, false -> previous.AdminPassword
        | true, true -> createSessionId()
      { previous with
          AdminPassword = pw
          UsersCanAccessPlayer = request.UsersCanAccessPlayer
          UsersCanAdd = request.UsersCanAdd
          UsersCanDelete = request.UsersCanDelete }

[<AutoOpen>]
module SessionExtensions =
  type Session.Sessions with
    member x.GetValue sessionId =
      match x with
      | Session.Sessions d ->
        match d.TryGetValue(sessionId) with
        | true, sess -> Some sess
        | _ -> None
    member x.Update sessionId oldSession newSession =
      match x with
      | Session.Sessions d when oldSession <> newSession ->
        let currentSession = d.AddOrUpdate (sessionId, (fun _ -> newSession), (fun _ old ->
          if old = oldSession then newSession else old))
        currentSession = newSession
      | Session.Sessions _ -> true
    member x.TryAdd sessionId newSession =
      match x with
      | Session.Sessions d -> d.TryAdd (sessionId, newSession)

//open Suave
//open Suave.Operators
//open Yaaf.Logging
//open Yaaf.Dj
//open Yaaf.Dj.SuaveHelpers

//module SuaveSession =
//  // Suave Helpers
//  let sessionUserStateKey = "session"
//  let sessionIdUserStateKey = "sessionId"
//  let sessionErrorsUserStateKey = "sessionErrors"
//  let addSession (sessId:string) (sess:Session.DjSession) =
//    SuaveHelpers.updatUserState (fun r ->
//      r.userState
//      |> Map.add sessionUserStateKey (sess:>obj)
//      |> Map.add sessionIdUserStateKey (sessId:>obj)
//      |> Map.add sessionErrorsUserStateKey (([]:Session.SessionError list):>obj))

//  let updateSession (sess:Session.DjSession) =
//    SuaveHelpers.updatUserState (fun r ->
//      r.userState
//      |> Map.remove sessionUserStateKey
//      |> Map.add sessionUserStateKey (sess:>obj))
//  let updateSessionErrors (errs:Session.SessionError list) =
//    SuaveHelpers.updatUserState (fun r ->
//      r.userState
//      |> Map.remove sessionErrorsUserStateKey
//      |> Map.add sessionErrorsUserStateKey (errs:>obj))

//  let userGivenSessionPasswordUserStateKey = "userGivenSessionPassword"
//  let tryAddPassword getPwd =
//    SuaveHelpers.updatUserState (fun r ->
//      let pwdM = getPwd r
//      match pwdM with
//      | Some (pwd:string) ->
//        r.userState
//        |> Map.add userGivenSessionPasswordUserStateKey (pwd:>obj)
//      | None ->
//        r.userState)

//  let addPasswordFromJson (pwdSelector:'a -> string) =
//    tryAddPassword (fun r ->
//      Suave.Json.fromJson r.request.rawForm
//      |> pwdSelector
//      |> Some)

//  let addPasswordFromBasicAuth =
//    let parseAuthenticationToken (token : string) =
//      let parts = token.Split (' ')
//      let enc = parts.[1].Trim()
//      let decoded = enc |> System.Convert.FromBase64String |> System.Text.Encoding.ASCII.GetString
//      let indexOfColon = decoded.IndexOf(':')
//      (parts.[0].ToLower(), decoded.Substring(0,indexOfColon), decoded.Substring(indexOfColon+1))
//    tryAddPassword (fun r ->
//      match r.request.header "authorization" with
//      | Choice1Of2 header ->
//        let (typ, username, password) = parseAuthenticationToken header
//        if (typ.Equals("basic") && username = "sessionAdmin") then
//          Some password
//        else
//          None
//      | Choice2Of2 _ -> None)

//  let getPassword (ctx:HttpContext) =
//    match ctx.userState.TryFind userGivenSessionPasswordUserStateKey with
//    | Some (:? string as pwd) ->
//      pwd
//    | _ ->
//      ""

//  let getSessionErrors (ctx:HttpContext) =
//    match ctx.userState.TryFind sessionErrorsUserStateKey with
//    | Some (:? (Session.SessionError list) as errs) ->
//      errs
//    | _ ->
//      []

//  let getAndAddSession (sessions:Session.Sessions) (sessId:string) =
//    context (fun context ->
//      let logger = getLogger context
//      if sessId.Contains("/") then
//        never
//      else
//        match sessions.GetValue sessId with
//        | Some sess -> addSession sessId sess
//        | _ ->
//          logger.logErr (fun _ -> L "No Session was found with the name '%s'!" sessId)
//          never)

//  let scanSession sessions (pf:PrintfFormat<_,_,_,_,_>) =
//    Filters.pathScan pf (getAndAddSession sessions)

//  let tryGetSession (ctx:HttpContext) =
//    match ctx.userState.TryFind sessionUserStateKey with
//    | Some (:? Session.DjSession as session) ->
//      match ctx.userState.TryFind sessionIdUserStateKey with
//      | Some(:? string as sessId) ->
//        Some (sessId, session)
//      | _ -> None
//    | _ ->
//      None
//  let getSession (ctx) =
//    let sess = tryGetSession ctx
//    match sess with
//    | Some s -> s
//    | None -> // Not possible
//      failwith "Session not found after scanSession!"

//  let getSessionResult (sessions:Session.Sessions) f handleResult =
//    context (fun r ->
//      let logger = SuaveHelpers.getLogger r
//      let session = tryGetSession r
//      match session with
//      | Some (sessId, sess) ->
//        let pwd = getPassword r
//        let resultR = f r pwd sess
//        let errors = Result.getErrors resultR @ getSessionErrors r
//        updateSessionErrors errors
//          >=> handleResult (sessId, sess, Result.asOption resultR)
//      | None ->
//        logger.logErr (fun _ -> L "No Session was found in the HttpContext userState!")
//        never
//    )

//  let transformSession (sessions:Session.Sessions) f =
//    getSessionResult sessions f (fun result ->
//      match result with
//      | sessId, sess, Some newSession ->
//        if sessions.Update sessId sess newSession then
//          updateSession newSession
//        else
//          context (fun c ->
//            let logger = getLogger c
//            logger.logWarn (fun _ -> L "Session could not be updated, because it was updated already!")
//            let errors = Session.SessionError.SessionModified :: getSessionErrors c
//            updateSessionErrors errors
//          )
//      | _ -> succeed
//    )

//  let handleErrors =
//    context (fun r ->
//      let logger = getLogger r
//      let session =
//        match tryGetSession r with
//        | Some (_, s) -> s
//        | None -> failwith "handleErrors has no session context"
//      let errs = getSessionErrors r
//      errs |> Seq.iter (function
//        | Session.UriError er ->
//          logger.logWarn (fun _ -> L "Uri error encountered: %A" er)
//        | Session.MyVideoError er ->
//          logger.logErr (fun _ -> L "MyVideo reported an error: %s" er)
//        | Session.YoutubeIdMissing er ->
//          logger.logErr (fun _ -> L "YoutubeId was missing in link: %s" er)
//        | Session.NoModuleCouldParseLink er ->
//          logger.logErr (fun _ -> L "No known module could parse the given link: %s" er)
//        | Session.Unauthorized ->
//          logger.logWarn (fun _ -> L "Access was denied because the user is not authorized")
//        | Session.SessionModified ->
//          logger.logErr (fun _ -> L "The session was modified by another request!")
//        | Session.BadRequest s ->
//          logger.logErr (fun _ -> L "Invalid Request: %s!" s))
//      match errs with
//      | Session.SessionModified :: _ ->
//        Response.response Http.HTTP_503
//          (sprintf "Session was modified, try again!"
//            |> System.Text.Encoding.UTF8.GetBytes)
//        >=> Writers.addHeader "Retry-After" "1" // Retry after 1 second
//      | Session.BadRequest s :: _ ->
//        RequestErrors.BAD_REQUEST s
//      | Session.Unauthorized :: _ ->
//        Authentication.authenticateBasic (fun (user, pass) -> user = "sessionAdmin" && pass = session.AdminPassword) <| context (fun c ->
//          logger.logErr (fun _ -> L "This error indicates that the authentication password was not properly saved in the userState!")
//          Response.response Http.HTTP_500
//            (sprintf "Authenticated but we couldn'd properly handle the request!"
//              |> System.Text.Encoding.UTF8.GetBytes)
//        )
//      | Session.NoModuleCouldParseLink _ :: _ ->
//        errs
//        |> Seq.choose (function Session.NoModuleCouldParseLink link -> Some link | _ -> None)
//        |> Seq.map (sprintf "The video link '%s' is invalid!")
//        |> String.concat "\n"
//        |> System.Text.Encoding.UTF8.GetBytes
//        |> Response.response Http.HTTP_400
//      | _ ->
//        never)

//  let onSuccess f =
//    handleErrors // Handle errors, for critical errors this already will produce a response
//                 // Otherwise this will return None and the next function will execute (<|>)
//    <|> Suave.Http.context (fun r ->
//      let _, s = getSession r
//      f s)