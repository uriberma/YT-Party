# YT.Party
Create your youtube Playlist and share it with others.

## Rest API (not yet implemented)

### Session Management

 - Create a Session

   ```
   POST: /api/createSession
   CONTENT: { "password": "mypassword", "sessionId": "ignored", "usersCanAdd": "false", "usersCanDelete": "false", "usersCanAccessPlayer": "false" }
   RESULT: { "password": "sessionPw", "sessionId": "slkdjg", "usersCanAdd": "false", "usersCanDelete": "false", "usersCanAccessPlayer": "false" }
   ```

   Create a new Session with the given password. Note that the `sessionId` parameter is ignored and the password might be overwritten if it is not acceptable.
   
 - Gets session information
   ```
   GET: /api/sessions/%s
   CONTENT:
   RESULT: { "password": "", "sessionId": "", "usersCanAdd": "false", "usersCanDelete": "false", "usersCanAccessPlayer": "false" }
   ```
   
   Needs Claim: `None` (everyone can access)

   Note: The password is never sent to the client with this request (it is always empty).
   The only case the client is allowed to see the password is when it is specified in the authorization header, but in this case there is no point in sending it (as the client already knows!).
   Just keep this in mind when using the return value to update the current state!

 - Update a Session
   ```
   PUT: /api/sessions/%s
   CONTENT: { "password": "mypassword", "sessionId": "ignored", "usersCanAdd": "false", "usersCanDelete": "false", "usersCanAccessPlayer": "false" }
   RESULT: { "password": "sessionPw", "sessionId": "unchanged", "usersCanAdd": "false", "usersCanDelete": "false", "usersCanAccessPlayer": "false" }
   ```
   
   Needs Claim: `Admin`

   As the sessionId is already specified in the url it is ignored in the request.
   Like in `createSession` the client needs to evaluate the returned password field as it might have been replaced (= not changed).

### Video Management
 
 - Add Videos
   ```
   POST: /api/sessions/%s/videos/addVideos
   CONTENT: [ { "videoLink": "https://www.youtube.com/watch?v=Ahwc-ouFeTQ", "startSeconds": 0, "endSeconds": 180 }, { "videoLink": "https://www.youtube.com/watch?v=Ahwc-ouFeTQ", "startSeconds": 0, "endSeconds": 180  } ]
   RESULT: [ { "videoId": "Ahwc-ouFeTQ", "startSeconds": 0, "endSeconds": 180, "id": "0" },{ "videoId": "Ahwc-ouFeTQ", "startSeconds": 0, "endSeconds": 180, "id": "1"  } ]
   ```

   Needs Claim: `UsersCanAdd`
   
   Adds the specified videos to the session. Returns the added items.

 - Remove all videos
   ```
   DELETE: /api/sessions/%s/videos
   CONTENT:
   RESULT: 200
   ```
   
   Needs Claim: `UsersCanRemove`

 - Remove a single videos
   ```
   DELETE: /api/sessions/%s/videos/%d
   CONTENT:
   RESULT: 200
   ```
   
   Needs Claim: `UsersCanRemove`

   %d is the id of the video to delete

 - Get the playlist
   ```
   GET: /api/sessions/%s/videos
   CONTENT:
   RESULT: [ { "videoId": "Ahwc-ouFeTQ", "startSeconds": 0, "endSeconds": 180, "id": "0" },{ "videoId": "Ahwc-ouFeTQ", "startSeconds": 0, "endSeconds": 180, "id": "1"  } ]
   ```
   
   Needs Claim: `None` (everyone can access)

   Return the current playlist of the session.
  
 - Randomize the playlist
   ```
   POST: /api/sessions/%s/videos/randomize
   CONTENT:
   RESULT: [ { "videoId": "Ahwc-ouFeTQ", "startSeconds": 0, "endSeconds": 180, "id": "1" },{ "videoId": "Ahwc-ouFeTQ", "startSeconds": 0, "endSeconds": 180, "id": "0"  } ]
   ```
   
   Needs Claim: `UsersCanAdd && UsersCanDelete`

   Return the new playlist of the session.
   NOTE: Because the PlayerPosition is saved as `id` the position will not change.
   
### Player Management
 
 - Get Player-Position
 
   ```
   GET: /api/sessions/%s/playerPos
   CONTENT:
   RESULT: { "playerPosition": "1" }
   ```

   Returns the `id` of the currently playing Video.

 - Set Player-Position
 
   ```
   PUT: /api/sessions/%s/playerPos
   CONTENT: { "playerPosition": "1" }
   RESULT: 200
   ```
   
   Update the player position. Use this API only when the user selected to play a specific song (ie. not the next one).
   Otherwise prefer the `nextPlayerPos` because this will keep multiple player in sync.
	
 - Play next song
 
   ```
   PUT: /api/sessions/%s/nextPlayerPos
   CONTENT: { "playerPosition": "1" }
   RESULT: { "playerPosition": "0" }
   ```
   
   Get the next song, note that this API checks if the current position is still the one sent to the server.
   If the current position doesn't match (For example because another player modified it).
   The server will send a 503 error code (try again). 
   In that case use the "Get Player-Position"-API to get notified about the next song.
   The client SHOULD consider to sync up the complete video-list if the next position (either via `nextPlayerPos` or `playerPos`) is not the local next one.
   The client MUST sync up the complete video-list if the next id is unknown.
   The client MUST sync up the complete video-list if the next id is given via `nextPlayerPos` and doesn't match the one expecting locally.

   
  

